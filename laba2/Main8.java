import java.util.Arrays;

class Main8
{

    public static void main(String[] args)
    {
        // Матрица N на M
        int[][] matrix2 =
                {
                        { 1, 2},
                        { 5, 6},
                        { 7, 4}
                };


        System.out.println("ИСХОДНАЯ МАТРИЦА:" );
        for (var m2: matrix2)
        {
            System.out.println( Arrays.toString(m2));
        }

        int [][]  matrix2_2 = rotate2(matrix2);//


        System.out.println("ПОВЕРНУЛИ МАТРИЦУ НА 90 ГРАДУСОВ ВЛЕВО:" );
        for (int [] r2: matrix2_2)
        {
            System.out.println(Arrays.toString(r2));
        }

    }

    public static int[][] rotate2(int[][] mas2)
    {
        int[][] res2 = new int[mas2[0].length][mas2.length];
        for (int i = 0; i < mas2.length; i++)
        {
            for (int j= mas2[0].length - 1; j >= 0; j--)
            {
                res2[mas2[0].length - 1 - j][i] = mas2[i][j];
            }
        }
        return res2;
    }

}


