import java.util.Arrays;
import java.util.IntSummaryStatistics;
public class Main7
{
    public static void main(String[] args)
    {
        int[][] massiv =
                {
                            {37, 2, 11},
                            {4,  7, 0},
                            {11, -11, 121}
                };

        System.out.println("Lab7: " + Arrays.toString(maximumElement(massiv)) + "\n");

    }
    public static int[] maximumElement(int[][] mas)
    {
        int[] res = new int[mas.length];
        for (int i = 0; i < mas.length; i++)
        {
            IntSummaryStatistics stats = Arrays.stream(mas[i]).summaryStatistics();
            res[i] = stats.getMax();
        }
        return res;
    }
}
