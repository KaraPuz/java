import java.util.Arrays;

class Main4
{

    public static void main(String[] args)
    {
        // Матрица N на M
        int[][] matrix1 =
            {
                    { 1, 2},
                    { 5, 6},
                    { 7, 4}
            };


        System.out.println("ИСХОДНАЯ МАТРИЦА:" );
        for (var m: matrix1)
        {
            System.out.println( Arrays.toString(m));
        }

        int [][]  matrix2_1 = rotate1(matrix1);//


        System.out.println("ПОВЕРНУЛИ МАТРИЦУ НА 90 ГРАДУСОВ ВПРАВО:" );
        for (int [] r1: matrix2_1)
        {
            System.out.println(Arrays.toString(r1));
        }

    }

   public static int[][] rotate1(int[][] mas1)
   {
        int[][] res1 = new int[mas1[0].length][mas1.length];
        for (int i = 0; i < mas1.length; i++)
        {
            for (int j = 0; j < mas1[0].length; j++)
            {
                res1[j][mas1.length - 1 - i] = mas1[i][j];
            }
        }
        return res1;
    }

}