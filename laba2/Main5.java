
 class Main5
{
    public static void main (String[] args)
    {
            int[] num = { 5, 6, 4, 3, 0, 9 };
            int target = 10;

            pairOfNumber(num, target);
    }
    public static void pairOfNumber(int[] num, int target) // метод поиска пары в массиве с заданной суммой
    {
            // рассмотрим каждый элемент, кроме последнего
        for (int i = 0; i < num.length - 1; i++)
        {
                // начинаем с i-го элемента до последнего элемента
            for (int j = i + 1; j < num.length; j++)
            {
                    // сравниваем пары
                if (num[i] + num[j] == target)
                {
                        //выводим найденную пару
                    System.out.printf("ПЕРВАЯ ПОДХОДЯЩАЯ ПАРА ЧИСЕЛ :" + "( "+  num[i] + " И " + num[j] + " )" );
                    return;
                }
            }
        }

            //  если пара не нашлась
        System.out.println("ПАРА НЕ НАШЛАСЬ :");
    }
}

