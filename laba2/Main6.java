
import java.util.Scanner;
public class Main6
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("ВВЕДИТЕ КОЛИЧЕСТВО СТРОК МАССИВА");
        int m = in.nextInt();
        System.out.println("ВВЕДИТЕ КОЛИЧЕСТВО СТОЛБЦОВ МАССИВА");
        int n = in.nextInt();
        int[][] mas = new int[m][n];

        //ввод массива M на N
        System.out.println("ВВЕДИТЕ СНАЧАЛА СТРОКУ МАССИВА");
        System.out.println("ПОТОМ ВВЕДИТЕ СТОЛБЦЫ МАССИВА ");
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                mas[i][j] = in.nextInt();
            }
        }

        int sum = 0;
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                sum += mas[i][j];
            }
        }
        System.out.println("СУММА ВВЕДЕННОГО ДВУМЕРНОГО МАССИВА:" + sum);

    }

}
