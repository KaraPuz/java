import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        //problem1();
        //problem2();
        //problem3();
        problem4();
        //problem5();


    }

    public static void problem1() {

        int shag = 0;
        int n = 10; // в этом месте можем поменять число
        while (n != 1) {
            if (n % 2 == 0) {
                n = n / 2;
            } else {
                n = 3 * n + 1;
            }
            shag++;
        }
        System.out.println(shag);//вывод строки с результатом
    }

    public static void problem2() {

        int summa = 0;//сумма знакочередующегося ряда
        int i = 1; //ориентировочное число(счетчик )

        // создаём новый объект ввода, через который будем дальше работать
        Scanner in = new Scanner(System.in);
        // выводим строку
        System.out.println("введите количество дальнейших чисел: \n ");
        // создаём переменную num и помещаем в неё то, что  ввели с клавиатуры
        int num = in.nextInt();
        // выводим ещё одну строку
        System.out.println("введите последовательность чисел через enter: \n");

        while (i <= num) { //следим,чтобы счетчик не был больше,чем общее число элементов
            if (i % 2 == 0) { //проверяем четность номера элемента

                summa -= in.nextInt();
            } else {
                summa += in.nextInt();
            }
            i++;
        }
        System.out.println(summa);//вывод строки с результатом

    }

    public static void problem3() {

        Scanner in = new Scanner(System.in);
        int flag = 0;
        int beginningX = 0;
        int beginningY = 0;

        System.out.println("введите координаты клада: \n");
        int finishX = in.nextInt();
        int finishY = in.nextInt();

        String stroka = "";//направление (с,ю,з,в)
        int move = 0; // количество шагов в определенном направлении,которое мы ввели

        int i = 0; //шаг //int count = 0;
        int I = 0;// конечное число шагов,потом сравним с i

        if (beginningX == finishX && beginningY == finishY) {
            System.out.println("вы находитесь на месте клада!!!УРА!!! \n");
        } else {


            while (true) {
                stroka = in.next();
                if (stroka.equals("стоп")) {
                    break;
                }
                move = in.nextInt();
                in.nextLine();
                if (stroka.equals("север")) {
                    beginningY += move;
                    i += 1;
                } else if (stroka.equals("юг")) {
                    beginningY -= move;
                    i += 1;
                } else if (stroka.equals("запад")) {
                    beginningX -= move;
                    i += 1;
                } else if (stroka.equals("восток")) {
                    beginningX += move;
                    i += 1;
                }

                if (beginningX == finishX && beginningY == finishY) {
                    flag += 1;
                    if (flag == 1) {
                        I = i;

                    }
                }
            }
        }
        System.out.println(I);
    }


    public static void problem4() {
        Scanner in = new Scanner(System.in);
        // System.out.println("введите количество дорог: \n");
        int min = 100000;
        int kolichestvoTunell = 0;
        int nomerDorogiResult = 0;
        int nomerTunellResult = 0;
        int nomerTunell = 0;
        int kolichestvoDorog = in.nextInt();//вводим количество дорог
        int[] minSize = new int[kolichestvoDorog];//кол-во дорог вводим в массив
        for (int i = 0; i < kolichestvoDorog; i++)  // переборираем дороги
        {
            kolichestvoTunell = in.nextInt();
            for (int j = 0; j < kolichestvoTunell; j++)  // перебираем тунели
            {
                nomerTunell = in.nextInt();
                if (nomerTunell < min) {
                    min = nomerTunell;
                }
            }
            minSize[i] = min;
            min = 100000;//снова обнуляем минимум,чтобы следующая дорога считалась корректно
        }
        for (int i = 0; i < kolichestvoDorog; i++)  // ищем максимум из минимумов
        {
            if (minSize[i] > nomerTunellResult) {
                nomerTunellResult = minSize[i];
                nomerDorogiResult = i + 1;
            }
        }
        System.out.println(nomerDorogiResult + " " + nomerTunellResult);
    }



    public static void problem5() {


        Scanner in = new Scanner(System.in);
        System.out.println("введите целое трехзначное число: \n");
        int num = in.nextInt();

        //проверка
        if (num > 999 || num < 100) {
            System.out.println("число введено неправильно\n") ;
        } else {

            //найдем разряды числа num
            int hundred = num / 100;
            int ten = num % 100 / 10;
            int one = num % 10;//берем остаток

            System.out.println(hundred);
            System.out.println(ten);
            System.out.println(one);

            if ((hundred + ten + one) % 2 == 0 && (hundred * ten * one) % 2 == 0) {
                System.out.println("число является дважды четным \n");
            } else {
                System.out.println("число не является дважды четным \n");
            }

        }


    }

}







